# Subscriber Weekly Email #

## Overview ##

## Dependancies ##
* Pentaho V5.4
* MySQL Java DB Driver
* Access to reporting DB - R/O
* Access to db to store job status - R/W
* Connection to Salesforce - Soap (std Pentaho plugin) - what are costs / performance concerns?
* Mandrill Webhook database (BrightTALK MySQL)
* PHP 5.5? TBC
* 3 Supporting Pentaho Jobs -- MySubscribedWeekly, GenerateKeywordAds, and CreateCommunityFeeds.  These must run immediately preceeding this job

## Schedule ##
Run once a week Sun 12:01am UTC

## Known Issues ##
* Deployment not yet automated - DL supporting BS 
* 

## Deployment ##
* Manual - code deployed from bitbucket
* need to create z:/SendWeeklyMySubscriber and z:/SendWeeklyMySubscriber/EmailSplits

### Starting the Job ###
* Start job from windows task schedular
* kitchen /job:"Create Ads for all Users" /level:basic /rep:"CalculateAds-dev" 
* - take care to not run twice

### Stopping the Job ###
* Kill job in windows task schedular

### Restarting the Job ###
* Contact BS - so we can attempt to restart from the correct step - otherwise
* Start / Resume job from windows task schedular (less efficient)


### Availability ###


### Housekeeping ###
* All temporary files written to Z: Drive (erased on reboot)


## Monitoring ##
* Alert if disk > 80%
* Alert if DB > 80%